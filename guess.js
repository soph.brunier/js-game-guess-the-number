let submitBtn = document.querySelector('#submit-btn');

let numberToGuess = Math.round(Math.random() * 100);

let success = false;

let gameDiv = document.querySelector('#form');

let clue = document.createElement('div');

let count = 1;



submitBtn.addEventListener('click', function() {
    let input = document.querySelector('#attempt');
    let answer = input.value;

    // if (isNaN(input)) {
    //     clue.textContent = 'This is not a number, try again';
    //     gameDiv.appendChild(clue);

    // } else
    if (answer < numberToGuess) {
        clue.textContent = `It's more than ${answer}`;
        gameDiv.appendChild(clue);

        count += 1;

        // console.log(`It's more than ${answer}`);
    } else if (answer > numberToGuess) {
        clue.textContent = `It's less than ${answer}`;
        gameDiv.appendChild(clue);

        count += 1;


        // console.log(`It's less than ${answer}`);
    } else {
        clue.textContent = `Yeah, you've found it!! The number was ${numberToGuess} ! You made it in ${count} turns`;
        gameDiv.appendChild(clue);

        success = true;
    }
});